# Refrii API

[![Build Status](https://travis-ci.org/mukopikmin/refrii-api.svg?branch=master)](https://travis-ci.org/mukopikmin/refrii-api)
[![Maintainability](https://api.codeclimate.com/v1/badges/735803a43f57d2785aa1/maintainability)](https://codeclimate.com/github/mukopikmin/refrii-api/maintainability)
[![Test Coverage](https://codeclimate.com/github/mukopikmin/refrii-api/badges/coverage.svg)](https://codeclimate.com/github/mukopikmin/refrii-api/coverage)
[![Dependency Status](https://gemnasium.com/badges/github.com/mukopikmin/refrii-api.svg)](https://gemnasium.com/github.com/mukopikmin/refrii-api)

## Requirement

 * Ruby 2.3+
